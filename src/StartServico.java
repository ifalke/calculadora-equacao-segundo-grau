import javax.xml.ws.Endpoint;

import service.CalculadoraWS;

public class StartServico {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/calculadora", new CalculadoraWS());
	}
}
