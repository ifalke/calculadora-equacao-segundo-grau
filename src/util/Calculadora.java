package util;

import exception.ArgumentoInvalidoException;
import exception.RaizRealInexistenteException;
import model.EquacaoSegundoGrauRequest;
import model.EquacaoSegundoGrauResponse;

public class Calculadora {
	
	private final Integer DOIS = 2;
	private final Integer QUATRO = 4;

	public EquacaoSegundoGrauResponse calcularEquacaoSegundoGrau(EquacaoSegundoGrauRequest request) throws RaizRealInexistenteException, ArgumentoInvalidoException {
		Integer delta = this.calcularDelta(request);
		
		if (delta < 0)
			throw new RaizRealInexistenteException("C�lculo n�o efetuado"); 
		
		return this.calcularResultadoEquacao(request, delta);
	}

	private EquacaoSegundoGrauResponse calcularResultadoEquacao(EquacaoSegundoGrauRequest request, Integer delta) {
		
		Integer x1 = (int) ( ((-request.getB()) + (Math.sqrt(delta))) / (DOIS * request.getA()) );
		Integer x2 = (int) ( ((-request.getB()) - (Math.sqrt(delta))) / (DOIS * request.getA()) );
		return new EquacaoSegundoGrauResponse(x1, x2);
	}

	private Integer calcularDelta(EquacaoSegundoGrauRequest request) throws ArgumentoInvalidoException {
		try {
			return (int) Math.pow(request.getB(), DOIS) - QUATRO * request.getA() * request.getC();
		} catch (NullPointerException e) {
			throw new ArgumentoInvalidoException("Argumento inv�lido ou nulo.");
		}
		
	}

}
