package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EquacaoSegundoGrauResponse {

	@XmlElement(required = true)
	private Integer xUm;
	@XmlElement(required = true)
	private Integer xDois;
	
	public EquacaoSegundoGrauResponse(Integer xUm, Integer xDois) {
		this.xUm = xUm;
		this.xDois = xDois;
	}

	EquacaoSegundoGrauResponse() {}

	public Integer getxUm() {
		return xUm;
	}
	public Integer getxDois() {
		return xDois;
	}
	
}
