package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EquacaoSegundoGrauRequest {

	@XmlElement(required = true, type = Integer.class)
	private Integer a;
	@XmlElement(required = true, type = Integer.class)
	private Integer b;
	@XmlElement(required = true, type = Integer.class)
	private Integer c;

	public EquacaoSegundoGrauRequest(Integer a, Integer b, Integer c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	EquacaoSegundoGrauRequest() {}

	public Integer getA() {
		return a;
	}

	public Integer getB() {
		return b;
	}

	public Integer getC() {
		return c;
	}

}
