package exception;

import javax.xml.ws.WebFault;

@WebFault(name = "ArgumentoInvalidoFault")
public class ArgumentoInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public ArgumentoInvalidoException(String mensagem) {
		super(mensagem);
	}
	
	public String getFaultInfo() {
		return "Favor, verificar os parāmetros passados";
	}
}
