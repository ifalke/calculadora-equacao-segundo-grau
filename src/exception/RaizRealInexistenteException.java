package exception;

import javax.xml.ws.WebFault;

@WebFault(name = "RaizRealInexistenteFault")
public class RaizRealInexistenteException extends Exception {

	private static final long serialVersionUID = 1L;

	public RaizRealInexistenteException(String mensagem) {
		super(mensagem);
	}
	
	public String getFaultInfo() {
		return "Delta com n�mero negativo, logo n�o existem raizes reais (x = 0)";
	}
}
