package service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.bind.annotation.XmlElement;

import exception.ArgumentoInvalidoException;
import exception.RaizRealInexistenteException;
import model.EquacaoSegundoGrauRequest;
import model.EquacaoSegundoGrauResponse;
import util.Calculadora;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
public class CalculadoraWS {

	@WebMethod(operationName = "CalcularEquacaoSegundoGrau")
	public EquacaoSegundoGrauResponse calcularEquacaoSegundoGrau(@XmlElement(required = true) @WebParam(name = "EquacaoSegundoGrau") EquacaoSegundoGrauRequest request) throws RaizRealInexistenteException, ArgumentoInvalidoException {
		return new Calculadora().calcularEquacaoSegundoGrau(request);
	}
}